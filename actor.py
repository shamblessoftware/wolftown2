import math
import random
import pygame
import pygame.freetype

from enum import Enum

class Frames(Enum):
    STAND = 0
    WALK1 = 1
    WALK2 = 2
    ANGRY = 3
    SURPRISE = 4
    SMIRK = 5
    DEAD = 6
    FACING_RIGHT = 0
    FACING_LEFT = 7

class Actions(Enum):
    IDLE = 0
    WALK = 1
    DEAD = 2
    ANGRY = 3
    SHOCK = 4
    HAPPY = 5

class Actor:
    def __init__(self, graphic_factory, name):
        self.name = name
        self.title = graphic_factory.generate_name(name)
        self.frames = graphic_factory.generate_person()
        self.dir = random.choice((Frames.FACING_LEFT, Frames.FACING_RIGHT))

        self.x = -max(self.title.get_width(), self.frames[0].get_width())
        self.y = random.randint(0, 720)
        self.vx = 0.0
        self.vy = 0.0
        self.area = pygame.Rect(100, 100, 700, 500)

        self.frame = random.choice([Frames.FACING_LEFT.value, Frames.FACING_RIGHT.value])
        self.start_action(Actions.WALK)
        self.speed = 110

    def start_action(self, action):
        if action == Actions.IDLE:
            self.t = random.uniform(8.0, 12.0)
            self.frame = self.dir.value + Frames.STAND.value
        elif action == Actions.WALK:
            self.t = 60.0
            self.tx = random.uniform(self.area.left, self.area.right)
            self.ty = random.uniform(self.area.top, self.area.bottom)
        elif action == Actions.DEAD:
            self.t = 600.0
            self.tx = self.x
            self.ty = self.y
            self.frame = self.dir.value + Frames.DEAD.value
        elif action == Actions.ANGRY:
            self.t = random.uniform(12.0, 15.0)
        elif action == Actions.SHOCK:
            self.t = 60
        elif action == Actions.HAPPY:
            self.t = random.uniform(1.5, 8.5)
            self.frame = self.dir.value + Frames.SMIRK.value

        self.action = action

    def set_area(self, area, walk):
        self.area = area
        if self.x < area.left or self.x > area.right or self.y < area.top or self.y > area.bottom:
            if walk:
                self.start_action(Actions.WALK)
            else:
                self.x = random.uniform(self.area.left, self.area.right)
                self.y = random.uniform(self.area.top, self.area.bottom)
                self.tx = self.x
                self.ty = self.y

    def become_ghost(self, graphic_factory, color):
        self.frames = graphic_factory.generate_ghost(color)
        self.speed = 280
        self.start_action(Actions.IDLE)

    def update(self, dt):
        self.t -= dt
        if self.t < 0.0:
            self.start_action(Actions.WALK)
        if self.action == Actions.WALK:
            self.vx = self.tx - self.x
            self.vy = self.ty - self.y
            l = math.sqrt(self.vx * self.vx + self.vy * self.vy)
            if l < 20.0:
                self.start_action(Actions.IDLE)
            else:
                movespeed = dt * self.speed / l
                self.x += movespeed * self.vx
                self.y += movespeed * self.vy
                f = int(self.t / 0.1) % 4
                self.dir = Frames.FACING_LEFT if self.x > self.tx else Frames.FACING_RIGHT
                self.frame = self.dir.value + [Frames.STAND, Frames.WALK1, Frames.STAND, Frames.WALK2][f].value
        elif self.action == Actions.ANGRY:
            f = int(self.t / 0.3) % 2
            self.frame = self.dir.value + [Frames.STAND, Frames.ANGRY][f].value
        elif self.action == Actions.SHOCK:
            if int(self.t/0.1) != int((self.t + dt)/0.1):
                self.dir = random.choice((Frames.FACING_LEFT, Frames.FACING_RIGHT))
                self.frame = self.dir.value + random.choice((Frames.STAND, Frames.SURPRISE)).value

    def render(self, screen):
        f = self.frames[self.frame]
        hx = self.x - f.get_width() / 2
        hy = self.y - f.get_height()
        screen.blit(f, (hx, hy))
        screen.blit(self.title, (self.x - self.title.get_width() / 2, hy - self.title.get_height()))

