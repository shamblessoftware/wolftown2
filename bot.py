import discord
from utils import get_number
from game import Game

class Bot(discord.Client):
    def __init__(self, scene, env):
        intents = discord.Intents.default()
        intents.members = True
        intents.message_content = True
        intents.voice_states = True
        super().__init__(intents=intents)

        self.game = Game(self, scene)
        self.env = env
        self.voice = None
        self.player_talk_list = []
        self.is_free_talk = True

    async def quit(self):
        await self.voice.disconnect()
        await self.close()

    async def speak(self, line):
        if not self.voice:
            self.voice = await self.channel_voice.connect()
        if self.voice.is_playing():
            self.voice.stop()
        self.voice.play(discord.FFmpegPCMAudio(f'audio/{line}.mp3'))

    async def dm(self, member, line):
        if not member.dm_channel:
            await member.create_dm()
        await member.dm_channel.send(line)

    async def channels_to_night_mode(self):
        #wolf channel allows talking
        #town channel disallows talking
        pass

    async def channels_to_day_mode(self):
        #wolf channel disallows talking
        #town channel allows talking
        pass

    async def channels_to_idle_mode(self):
        #disallow talking
        pass

    async def channel_talk_permissions(self):
        pass

    async def clear_channel(self, channel):
        for m in channel.members:
            await channel.set_permissions(m, overwrite=None)

    async def add_player_to_channel(self, channel, member, read_only):
        p = channel.permissions_for(member)
        if not p.read_messages or p.send_messages is read_only:
            await channel.set_permissions(member, read_messages=True, send_messages=not read_only)

    async def remove_player_from_channel(self, channel, member):
        p = channel.permissions_for(member)
        if p.read_messages or p.send_messages:
            await channel.set_permissions(member, overwrite=None)

    async def limit_talking_players(self, players):
        self.player_talk_list = players
        self.is_free_talk = False
        for m in self.channel_voice.members:
            should_mute = m != self.user and m not in players
            if m.voice.mute != should_mute:
                await m.edit(mute=should_mute)

    async def mute_specific_player(self, player):
        if player.voice and not player.voice.mute:
            await player.edit(mute=True)

    async def free_talk(self):
        self.is_free_talk = True
        for m in self.channel_voice.members:
            await m.edit(mute=False)
            
    async def reset(self):
        if not self.voice:
            self.voice = await self.channel_voice.connect()
        await self.free_talk()
        await self.clear_channel(self.channel_town)
        await self.clear_channel(self.channel_mason)
        await self.clear_channel(self.channel_wolf)
        await self.clear_channel(self.channel_dead)
        
    async def on_ready(self):
        for g in self.guilds:
            if g.name == self.env.DISCORD_GUILD:
                self.guild = g
                break

        for c in self.guild.channels:
            if c.name == self.env.CHANNEL_VOICE:
                self.channel_voice = c
            if c.name == self.env.CHANNEL_TOWN:
                self.channel_town = c
            if c.name == self.env.CHANNEL_MASON:
                self.channel_mason = c
            if c.name == self.env.CHANNEL_WOLF:
                self.channel_wolf = c
            if c.name == self.env.CHANNEL_DEAD:
                self.channel_dead = c

        for r in self.guild.roles:
            if r.name == self.env.GM_ROLE:
                self.gm_role = r

        print(
            f'{self.user} is connected to the following guild:\n'
            f'{self.guild.name} (id: {self.guild.id})\n'
            f'Voice channel: {self.channel_voice.name} (id: {self.channel_voice.id})\n'
            f'Town channel: {self.channel_town.name} (id: {self.channel_town.id})\n'
            f'Mason channel: {self.channel_mason.name} (id: {self.channel_mason.id})\n'
            f'Wolf channel: {self.channel_wolf.name} (id: {self.channel_wolf.id})\n'
            f'Dead channel: {self.channel_dead.name} (id: {self.channel_dead.id})\n'
        )

        await self.reset()
        print('=== READY ===')

    async def on_message(self, message):
        if message.author == self.user:
            return

        if message.channel.type == discord.ChannelType.private:
            await self.game.dm_respond(message.author, message.clean_content)
            return

        if message.channel == self.channel_town:
            if message.content.startswith('!nom '):
                await self.game.vote_nominate(message.author, get_number(message.content) - 1)

            if message.content == '!sleep':
                await self.game.vote_sleep(message.author)

            if message.content == '!kill':
                await self.game.vote_execution(message.author, True)

            if message.content == '!save':
                await self.game.vote_execution(message.author, False)

        if message.channel == self.channel_wolf:
            if message.content.startswith('!slay '):
                await self.game.vote_slay(get_number(message.content) - 1)

        # ADMIN(?) COMMANDS
        if self.gm_role in message.author.roles:
            if message.content == "!reset":
                await self.reset()

            if message.content == '!quit':
                self.game.scene.running = False
                await self.free_talk()
                await self.quit()

            if message.content.startswith('!join'):
                for p in set(message.mentions):
                    self.game.join(p)

            if message.content.startswith('!leave'):
                for p in set(message.mentions):
                    await self.game.leave(p)

            if message.content == '!start':
                await self.game.start()

        # LOBBY COMMANDS
        if message.content == '!join':
            self.game.join(message.author)

        if message.content == '!leave':
            await self.game.leave(message.author)

        if message.content == '!help':
            pass

    async def on_voice_state_update(self, member, before, after):
        if member == self.user:
            return
        # just left?
        if before.channel == self.channel_voice and after.channel and after.channel != self.channel_voice:
            await member.edit(mute=False)
        # just joined?
        if before.channel != self.channel_voice and after.channel == self.channel_voice:
            await member.edit(mute=(not self.is_free_talk and member not in self.player_talk_list))

