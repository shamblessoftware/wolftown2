```ini
[ How to play ]
```
**Lobby**

Type the following commands:

 • !join - join the game
 • !leave - change your mind and leave the game
** **

**Game**
Everyone in the game is assigned a role either on the village team, or the werewolf team, or as a neutral player. Villagers want to rid the town of werewolves. Werewolves want to kill enough villagers so that they outnumber them. Neutrals have their own individual goals.

The day starts on a 'dawn' phase, then progresses between 'day' and 'night' phases alternately.
** **

:sunny: **Day**
During the day, players may attempt to execute other players. A player gets nominated to stand trial for execution, then the town votes on whether to kill or save them. Day phase continues until a player is successfully executed, or until three nominations have been made without an execution, or until no nomination is made within the time limit.

Each player gets a nomination token. They can use this to nominate a player. The more living players there are, the more nomination tokens are required for a player to stand trial and face a vote. Every four players, an additional nomination token is required. For example, if there are eight or more living players, two tokens would be needed to nominate a person. If there are twelve living players, three tokens would need to nominate.

Once a player is successfully put to trial and voted on (even if the vote is unsuccessful), the players who nominated them lose their nomination tokens, and get them back the day after next. If there are four or fewer players, everyone's nomination tokens are restored every night.

If players want to skip the day phase, they can type !sleep in the day chat. If enough players want to sleep, the day phase will immediately come to a conclusion.
** **

:new_moon: **Night**
During the night, all players are muted. The bot will coordinate role-specific special abilities via DM.

Certain text channels may be available at night. For example, the werewolves have a channel where they can decide who to attempt to kill using the !slay command.
** **

:sunrise_over_mountains: **Dawn**
The first phase of the game is 'dawn'. It is the same as 'night', but no killing takes place, and most roles do not operate. The seer, detective and town crier can all act during dawn, and the wolves and masons can use their night time channels.
** **

