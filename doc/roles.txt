```ini
[ Roles in the game ]
```
:homes: **Village team**
 • Villager: No special abilities.
 • Seer: Finds out the alignment (village, werewolf or neutral) of other players.
 • Knight: Protects players from death. Can't protect the same player twice in a row.
 • Gravedigger: Finds out the role of dead players.
 • Detective: Finds players with roles that are active at night.
 • Mason: Hangs out with other masons.
 • Hunter: When executed, kills the first player who nominated them.
 • Judge: Votes count twice.
 • Bureaucrat: Knows two (non-obvious) roles that exist.
 • Medium: Talks to the dead at night.
 • Priest: Talks anonymously to other players at night.
 • Fraud: Thinks they are a seer, but gets random results.
 • Lycan: The seer sees them as a werewolf.
 • Hero: Lasts an additional day after being killed at night.
 • Vigilante: Can kill others at night, but dies of shame if they kill a village player.
** **

:wolf: **Werewolf team**
 • Werewolf: No special abilities.
 • Minion: Finds out the roles of players.
 • Town Crier: Can send anonymous propaganda to the town.
 • Traitor: Appears on the village team, but wins with the wolves.
 • Rapscallion: Learns a village role that is not in play.
 • Alpha: Has a one-time only bonus kill.
** **

:person_walking: **Neutrals**
 • Tanner: Wins if executed during the day. Those who nominated the execution can't nominate for two days.
 • Witch: Can heal or poison others. Wins if they survive.
 • Gambler: Wins if they correctly predict two executions.
 • King: Can investigate roles and pardon their own execution. Wins if they survive.
 • Romeo and Juliet: Neutrals disguised as a villager/werewolf pair. Win exclusively if they kill all other players.
