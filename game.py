import math
import asyncio
import discord
from enum import Enum
from scene import *
from roles import *
from person import *
from actor import Actions
from roleselection import select_roles

class State(Enum):
    LOBBY = 1
    DAWN = 2
    DAY = 3
    NOMINATION = 4
    VOTING = 5
    CUTSCENE = 6
    NIGHT = 7

class Town:
    pass

MIN_PLAYERS = 4

class Game:
    def __init__(self, discord, scene):
        self.discord = discord
        self.state = State.LOBBY
        self.scene = scene
        self.players = list()
        self.town = list()
        self.sleepers = list()
        self.nomination_counter = 0
        self.nomination_today = 1
        self.stalemate_counter = 0
        self.wolf_attack = -1

    # Role handling

    def town_for_player(self, player):
        targets = [t for t in self.town if t.player == player]
        if targets:
            return targets[0]
        return None

    async def dm_respond(self, player, message):
        if self.state not in (State.DAWN, State.NIGHT):
            return
        who = self.town_for_player(player)
        if not who or not who.alive:
            return
        if self.state == State.DAWN and not who.person.dawn:
            return
        await who.person.night_respond(who.player.dm_channel, self.town, message)

    # Lobby-related commands

    def join(self, player):
        if self.state is not State.LOBBY:
            return
        if not self.players:
            self.scene.set_scene(SceneType.FORK)
        if player not in self.players:
            self.players.append(player)
            self.scene.add_actor(player.id, player.display_name, AREA_OFF_LEFT)
            self.scene.set_actor_walk_area([player.id], AREA_FORK)
            self.update_player_count()

    async def leave(self, player):
        if self.state is not State.LOBBY:
            return
        if player in self.players:
            self.players.remove(player)
            self.scene.set_actor_walk_area([player.id], AREA_OFF_RIGHT)
            self.update_player_count()

    def update_player_count(self):
        self.scene.clear_text()
        p = len(self.players)
        if p >= MIN_PLAYERS:
            self.scene.add_text(0.35, 0.8, 0.3, 0.2, (0xFF, 0xFF, 0x40), f"{p} players")

    async def assign_players_to_channels(self, daytime):
        for t in self.town:
            if not t.alive:
                await self.discord.add_player_to_channel(self.discord.channel_town, t.player, True)
                await self.discord.remove_player_from_channel(self.discord.channel_mason, t.player)
                await self.discord.remove_player_from_channel(self.discord.channel_wolf, t.player)
                await self.discord.add_player_to_channel(self.discord.channel_dead, t.player, False)
            else:
                await self.discord.add_player_to_channel(self.discord.channel_town, t.player, not daytime)
                if t.person.mason_channel:
                    await self.discord.add_player_to_channel(self.discord.channel_mason, t.player, daytime)
                else:
                    await self.discord.remove_player_from_channel(self.discord.channel_mason, t.player)
                if t.person.wolf_channel:
                    await self.discord.add_player_to_channel(self.discord.channel_wolf, t.player, daytime)
                else:
                    await self.discord.remove_player_from_channel(self.discord.channel_wolf, t.player)
                if t.role == Role.MEDIUM and not daytime:
                    await self.discord.add_player_to_channel(self.discord.channel_dead, t.player, False)
                else:
                    await self.discord.remove_player_from_channel(self.discord.channel_dead, t.player)

    async def remove_players_from_channels(self):
        for t in self.town:
            if t.alive:
                if t.person.mason_channel:
                    await self.discord.remove_player_from_channel(self.discord.channel_mason, t.player)
                if t.person.wolf_channel:
                    await self.discord.remove_player_from_channel(self.discord.channel_wolf, t.player)
                if t.role == Role.MEDIUM:
                    await self.discord.remove_player_from_channel(self.discord.channel_dead, t.player)

    async def start(self):
        if len(self.players) < MIN_PLAYERS:
            return
        self.scene.clear_text()
        self.state = State.DAWN
        self.nomination_counter += 1
        actors = [x.id for x in self.players]
        self.scene.filter_actors(actors)
        self.scene.set_actor_walk_area(actors, AREA_OFF_TOP)
        await asyncio.sleep(3)
        self.scene.set_scene(SceneType.DAWN)
        self.scene.filter_actors([])
        # move players to outside of town
        await self.discord.speak('dawn')
        
        # pick roles
        self.roles = select_roles(len(self.players))
        random.shuffle(self.roles)
        random.shuffle(self.players)
        
        for t in zip(self.players, self.roles):
            town = Town()
            town.player = t[0]
            town.name = town.player.display_name
            town.role = t[1]
            town.person = Person.factory[town.role]()
            town.alive = True
            town.hp = 0
            town.nomination_points = 1
            town.nominating = None
            town.nominated_by = list()
            self.town.append(town)

        await self.discord.limit_talking_players([])
        await self.discord.channel_town.send(":homes: Welcome to the town...")
        if [t for t in self.town if t.person.mason_channel]:
            await self.discord.channel_mason.send(":handshake: Greetings, fellows of the lodge...")
        await self.discord.channel_wolf.send(":evergreen_tree: In a clearing in the forest...")
        await self.discord.channel_dead.send(":ghost: The dead are restless...")
        await self.assign_players_to_channels(False)
        for t in self.town:
            await self.discord.dm(t.player, t.person.intro)
            if t.person.dawn:
                await t.person.night_begin(t.player.dm_channel, self.town)

        self.scene.set_timer(45)
        await asyncio.sleep(45)

        for t in self.town:
            if t.person.dawn:
                await t.person.night_end(t.player.dm_channel, self.town)

        await self.start_day_cycle()

    async def start_day_cycle(self):
        # announce deaths
        for t in self.town:
            if t.alive and t.hp < 0:
                # Any form of attack resets stalemate
                self.stalemate_counter = 0
                await self.dead_player(t)
            t.hp = 0
       
        if self.check_game_end():
            await self.run_game_end()
            return

        for t in self.town:
            if t.alive and t.role == Role.CRIER and t.person.message:
                self.scene.set_scene(SceneType.SCROLL)
                self.scene.filter_actors([])
                self.scene.add_text(0.2, 0.12, 0.65, 0.65, (0x80, 0, 0), t.person.message)
                await self.discord.speak('crier')
                await self.discord.channel_town.send(f':bell: The town crier says "_{t.person.message}_"')
                await asyncio.sleep(10)
                self.scene.clear_text()

        self.scene.set_scene(SceneType.TOWN)
        self.scene.filter_actors([])
        await self.discord.speak('daybreak')
        await self.assign_players_to_channels(True)

        choices = [(n, t) for n, t in enumerate(self.town, start=1) if t.alive]
        self.nomination_requirement = max(1, math.floor(len(choices) / 4))
        if len(choices) <= 4:
            for t in self.town:
                t.nomination_points = 1
        can_nom = ':coin:'
        cant_nom = ''
        choices_text = [f'**{n}**: {t.name} {can_nom if t.nomination_points == 1 else cant_nom}' for n, t in choices]
        await self.discord.channel_town.send(f":sunny: Good morning. {self.nomination_requirement} nominations required for a vote. Players that can be **!nom**inated are:\n" + "\n".join(choices_text))
        await self.discord.channel_wolf.send(":shushing_face: It's morning.")

        # set up scene
        actors = [t.player.id for t in self.town if t.alive]
        for a in actors:
            self.scene.set_actor_location([a], random.choice([AREA_OFF_LEFT, AREA_OFF_RIGHT, AREA_OFF_TOP, AREA_OFF_BOTTOM]))
        self.scene.set_actor_walk_area(actors, AREA_TOWN)
        self.scene.filter_actors(actors)

        # unmute, give write permissions to living players
        await self.discord.limit_talking_players([t.player for t in self.town if t.alive])

        self.state = State.DAY

        # initialize and update a fallback task
        self.nomination_today = 1
        await self.continue_day_cycle(180)

    async def continue_day_cycle(self, timeout):
        if self.nomination_today >= 4:
            self.state = State.CUTSCENE
            self.stalemate_counter += 1
            await self.discord.speak('stalemate')
            await asyncio.sleep(3)
            await self.start_night_cycle(True)
            return

        if len([t for t in self.town if t.alive and t.nomination_points == 1]) < self.nomination_requirement:
            self.state = State.CUTSCENE
            self.stalemate_counter += 1
            await self.discord.speak('no-coin')
            await asyncio.sleep(7)
            await self.start_night_cycle(True)
            return

        self.state = State.DAY
        counter = self.nomination_counter
        self.sleepers = list()
        self.scene.set_timer(timeout)
        await asyncio.sleep(timeout)
        if counter == self.nomination_counter:
            self.state = State.CUTSCENE
            self.stalemate_counter += 1
            await self.discord.speak('peace')
            await asyncio.sleep(3)
            await self.start_night_cycle(True)        

    async def vote_nominate(self, member, number):
        if self.state is not State.DAY:
            return
        who = self.town_for_player(member)
        if not who or not who.alive:
            return
        if number == -1:
            if who.nominating:
                who.nominating.nominated_by.remove(who)
                who.nominating = None
                await self.discord.channel_town.send(f"{member.display_name} has cancelled their nomination.")
            return
        if number < 0 or len(self.town) <= number:
            return
        target = self.town[number]
        if not self.town[number].alive:
            return
        if who == target:
            return
        if who.nomination_points < 1:
            return
        if who.nominating and who.nominating == target:
            return

        # is this nomination going to trigger a vote?
        trigger_vote = len(target.nominated_by) + 1 >= self.nomination_requirement
        # pre-emptively block nomination race condition
        continuation_timer = self.scene.get_timer() + 30
        if trigger_vote:
            self.state = State.NOMINATION
            self.scene.clear_timer()
            self.nomination_counter += 1

        # Sort out nomination pointers
        target.nominated_by.append(who)
        ratio = f"({len(target.nominated_by)}/{self.nomination_requirement})"
        if who.nominating:
            who.nominating.nominated_by.remove(who)
            await self.discord.channel_town.send(f"{member.display_name} instead nominates **{target.name}** {ratio}")
        else:
            await self.discord.channel_town.send(f"{member.display_name} has nominated **{target.name}** {ratio}")
        who.nominating = target
        self.scene.set_actor_action([who.player.id], Actions.ANGRY)
        if not trigger_vote:
            return

        for t in self.town:
            t.nominating = None
            if t is not target:
                t.nominated_by = list()

        for p in target.nominated_by:
            # paranoid nominator check
            p.nomination_points -= 2

        self.scene.set_actor_walk_area([target.player.id], AREA_NOMINATED)
        self.voters = [t for t in self.town if t.alive and t != target]
        self.scene.set_actor_walk_area([v.player.id for v in self.voters], AREA_CROWD)

        # mute everyone
        await self.discord.limit_talking_players([])
        await self.discord.speak(f'nomination{self.nomination_today}')
        await asyncio.sleep(3)

        #unmute target
        await self.discord.limit_talking_players([target.player])
        self.scene.set_actor_action([target.player.id], Actions.ANGRY)
        await asyncio.sleep(15)

        self.voted_kill = []
        self.voted_save = []
        self.state = State.VOTING
        #unmute everyone
        await self.discord.channel_town.send(f":ballot_box: Vote for the life of {target.name} with either **!kill** or **!save**");
        await self.discord.limit_talking_players([t.player for t in self.town if t.alive])
        await self.discord.speak('vote')
        self.scene.add_text(0.05, 0.1, 0.15, 0.1, (0xFF, 0x0, 0x0), "!kill")
        self.scene.add_text(0.8, 0.1, 0.15, 0.1, (0xFF, 0xFF, 0x0), "!save")

        # accept votes now
        self.scene.set_timer(30)
        await asyncio.sleep(30)
        self.state = State.CUTSCENE
        # force votes on undecided
        undecided = [t for t in self.voters if t not in self.voted_kill and t not in self.voted_save]
        if undecided:
            self.voted_save += undecided
            self.scene.set_actor_walk_area([t.player.id for t in undecided], AREA_VOTE_NO)
            await asyncio.sleep(2)
        await asyncio.sleep(1)
        self.scene.clear_text()
        
        voted_kill_list = [t.name for t in self.voted_kill]
        voted_save_list = [t.name for t in self.voted_save]
        judge_kill_list = [t for t in self.voted_kill if t.role == Role.JUDGE]
        judge_save_list = [t for t in self.voted_save if t.role == Role.JUDGE]

        if len(voted_kill_list) + len(judge_kill_list) <= len(voted_save_list) + len(judge_save_list):
            voters_text = ", ".join(voted_save_list)
            if len(voted_kill_list) <= len(voted_save_list):
                await self.discord.channel_town.send(f":dove: Without a majority, the opposition (**{voters_text}**) agree {target.name} is free to go.")
                await self.discord.speak('clemency')
            else:
                await self.discord.channel_town.send(f":scales: Due to the Judge, the opposition (**{voters_text}**) agree {target.name} is free to go.")
                await self.discord.speak('clemency-judge')
            target.nominated_by = list()
            self.scene.set_actor_walk_area([t.player.id for t in self.town if t.alive], AREA_TOWN)
            self.scene.set_actor_action([t.player.id for t in self.voted_save], Actions.HAPPY)
            self.nomination_today += 1
            await self.continue_day_cycle(continuation_timer)
            return

        voters_text = ", ".join(voted_kill_list)
        if len(voted_kill_list) > len(voted_save_list):
            await self.discord.channel_town.send(f":skull_crossbones: The majority (**{voters_text}**) voted to execute {target.name}")
            await self.discord.speak('execute')
        else:
            await self.discord.channel_town.send(f":scales: Due to the Judge, the majority (**{voters_text}**) voted to execute {target.name}")
            await self.discord.speak('execute-judge')

        await asyncio.sleep(2)

        if target.role == Role.HUNTER and target.nominated_by:
            await asyncio.sleep(1)
            await self.discord.speak('shoot')
            victim = target.nominated_by[0]
            await self.set_player_death_state(victim)
            self.scene.set_actor_action([target.player.id], Actions.ANGRY)
            self.scene.set_actor_action([victim.player.id], Actions.DEAD)
            await asyncio.sleep(2)
            await self.discord.speak('hunter')
            await asyncio.sleep(4)
            self.scene.kill_actor(victim.player.id, victim.person.alignment.value[2])
            self.scene.set_actor_walk_area([victim.player.id], AREA_OFF_RIGHT)
            await self.announce_player_alignment(victim)
            if self.check_game_end():
                await self.run_game_end()
                return

        if target.role == Role.KING and target.person.pardon:
            target.person.pardon = False
            await asyncio.sleep(1)
            await self.discord.speak('king')
            await self.discord.channel_town.send(f":crown: {target.name} pardons their own execution.")
            await asyncio.sleep(6)
            self.scene.set_actor_walk_area([t.player.id for t in self.town if t.alive], AREA_TOWN)
            self.scene.set_actor_action([target.player.id], Actions.HAPPY)
            self.nomination_today += 1
            await self.continue_day_cycle(continuation_timer)
            return

        if target.role == Role.TANNER:
            target.person.win = True
            for p in target.nominated_by:
                p.nomination_points -= 1

        for t in self.town:
            if t.role == Role.GAMBLER:
                if t.person.mark is target:
                    t.person.score += 1
                    t.person.win = t.person.score >= 2
                t.person.mark = None

        mob = [t for t in self.town if t.alive and t is not target]
        mob_ids = [t.player.id for t in mob]
        self.scene.set_actor_walk_area(mob_ids, AREA_LYNCH)
        self.scene.set_actor_action([target.player.id], Actions.SHOCK)
        await asyncio.sleep(5)
        await self.discord.speak('hit')
        await self.set_player_death_state(target)
        self.scene.set_actor_action([target.player.id], Actions.DEAD)
        self.scene.set_actor_walk_area(mob_ids, AREA_OFF_TOP)
        await asyncio.sleep(3)
        self.scene.kill_actor(target.player.id, target.person.alignment.value[2])
        self.scene.set_actor_walk_area([target.player.id], AREA_OFF_RIGHT)
        await self.announce_player_alignment(target)

        if self.check_game_end():
            await self.run_game_end()
        else:
            await self.start_night_cycle(False)
        
    async def vote_execution(self, member, kill):
        if self.state is not State.VOTING:
            return
        who = self.town_for_player(member)
        if not who or not who.alive:
            return
        if who not in self.voters:
            return
        if kill and who not in self.voted_kill:
            if who in self.voted_save:
                self.voted_save.remove(who)
            self.voted_kill.append(who)
            self.scene.set_actor_walk_area([member.id], AREA_VOTE_YES)
        elif not kill and who not in self.voted_save:
            if who in self.voted_kill:
                self.voted_kill.remove(who)
            self.voted_save.append(who)
            self.scene.set_actor_walk_area([member.id], AREA_VOTE_NO)

    async def vote_sleep(self, player):
        if self.state is not State.DAY:
            return
        t = self.town_for_player(player)
        if not t or not t.alive:
            return
        if t in self.sleepers:
            return
        self.sleepers.append(t)
        coins = len([t for t in self.town if t.alive and t.nomination_points == 1 and not t in self.sleepers])
        if coins < self.nomination_requirement:
            self.state = State.CUTSCENE
            self.nomination_counter += 1
            await self.discord.speak('sleep')
            await asyncio.sleep(3)
            await self.start_night_cycle(True)

    async def vote_slay(self, number):
        if self.state is not State.NIGHT:
            return
        if number == self.wolf_attack:
            return
        if number == -1:
            await self.discord.channel_wolf.send("You will not attack tonight.")
            self.wolf_attack = -1
            return
        if number < 0 or len(self.town) <= number:
            return
        target = self.town[number]
        if not target.alive or target.person.wolf_channel:
            return
        self.wolf_attack = number
        await self.discord.channel_wolf.send(f"You will attack **{target.name}** tonight.")

    async def start_night_cycle(self, walk_away):
        self.state = State.NIGHT

        if walk_away:
            for a in [t.player.id for t in self.town if t.alive]:
                self.scene.set_actor_walk_area([a], random.choice([AREA_OFF_LEFT, AREA_OFF_RIGHT, AREA_OFF_TOP, AREA_OFF_BOTTOM]))
            await asyncio.sleep(5)

        await self.discord.limit_talking_players([])
        await self.discord.speak('night')
        await self.assign_players_to_channels(False)
        await self.discord.channel_town.send(":crescent_moon: It is night time");
        targets = [f'**{n}**: {t.name}' for n, t in enumerate(self.town, start=1) if t.alive and not t.person.wolf_channel]
        menu = '\n'.join(targets)
        self.wolf_attack = -1
        await self.discord.channel_wolf.send(f":full_moon: It is night time. Select a player to **!slay**...\n{menu}");
        await asyncio.sleep(2)

        dead = [t.player.id for t in self.town if not t.alive]
        if dead:
            self.scene.set_scene(SceneType.GRAVEYARD)
            self.scene.set_actor_location(dead, AREA_TOWN)
            self.scene.filter_actors(dead)
        else:
            self.scene.set_scene(SceneType.MOON)
            self.scene.filter_actors([])

        living = [t for t in self.town if t.alive]
        for t in living:
            if t.nomination_points < 1:
                t.nomination_points += 1
            await t.person.night_begin(t.player.dm_channel, self.town)

        self.scene.set_timer(60)
        await asyncio.sleep(60)

        # Lock channels
        await self.remove_players_from_channels()

        # Apply wolf damage
        if self.wolf_attack != -1:
            self.town[self.wolf_attack].hp -= 1

        # Close night actions
        for s in Sequence:
            for t in living:
                await t.person.night_end(s, t.player.dm_channel, self.town)

        await self.start_day_cycle()

    async def dead_player(self, target):
        fake = target.role == Role.HERO and not target.person.dying
        if fake:
            target.person.dying = True
        else:
            await self.set_player_death_state(target)

        # show random scene
        self.scene.set_scene(SceneType.BLACK)
        self.scene.filter_actors([target.player.id])
        self.scene.set_actor_location([target.player.id], AREA_TOWN)
        self.scene.set_actor_action([target.player.id], Actions.DEAD)
        await asyncio.sleep(1)
        await self.discord.speak('hero' if fake else 'died')
        await asyncio.sleep(3)

        if fake:
            self.scene.set_actor_walk_area([target.player.id], AREA_OFF_LEFT)
            await self.discord.channel_town.send(f":drop_of_blood: {target.name} has been critically wounded.")
            await asyncio.sleep(4)
            return

        self.scene.kill_actor(target.player.id, target.person.alignment.value[2])
        self.scene.set_actor_walk_area([target.player.id], AREA_OFF_RIGHT)
        await asyncio.sleep(1)
        await self.announce_player_alignment(target)

    async def set_player_death_state(self, target):
        target.alive = False
        await self.discord.mute_specific_player(target.player)
        self.stalemate_counter = 0
        if target.role in (Role.WITCH, Role.KING):
            target.person.win = False
        await self.discord.add_player_to_channel(self.discord.channel_town, target.player, True)
        await self.discord.add_player_to_channel(self.discord.channel_dead, target.player, False)
        # remove from other channels
        await self.discord.remove_player_from_channel(self.discord.channel_mason, target.player)
        await self.discord.remove_player_from_channel(self.discord.channel_wolf, target.player)

    async def announce_player_alignment(self, target):
        await asyncio.sleep(1)
        if target.person.alignment == Alignment.VILLAGER:
            await self.discord.speak('player-good')
            await self.discord.channel_town.send(f":skull: {target.name} has been killed, and was on the **village team**.")
        elif target.person.alignment == Alignment.WEREWOLF:
            await self.discord.speak('player-evil')
            await self.discord.channel_town.send(f":skull: {target.name} has been killed, and was on the **werewolf team**.")
        else:
            await self.discord.speak('player-neutral')
            await self.discord.channel_town.send(f":skull: {target.name} has been killed, and was **neutral**.")
        await asyncio.sleep(5)

    def check_game_end(self):
        wolves = [t for t in self.town if t.alive and (t.person.parity_alignment or t.person.alignment) == Alignment.WEREWOLF]
        town = [t for t in self.town if t.alive and (t.person.parity_alignment or t.person.alignment) == Alignment.VILLAGER]

        return not wolves or len(wolves) >= len(town) or self.stalemate_counter >= 3

    async def run_game_end(self):
        wolves = [t for t in self.town if t.alive and (t.person.parity_alignment or t.person.alignment) == Alignment.WEREWOLF]

        winning_alignment = Alignment.WEREWOLF if wolves else Alignment.VILLAGER
        losing_alignment = Alignment.VILLAGER if wolves else Alignment.WEREWOLF

        self.scene.set_scene(SceneType.WINNERS)
        winners = [t for t in self.town if (t.person.win_alignment or t.person.alignment) == winning_alignment]
        losers = [t for t in self.town if (t.person.win_alignment or t.person.alignment) == losing_alignment]

        # Romeo/juliet handling
        if len(wolves) == 1:
            living = [t for t in self.town if t.alive]
            lovers = [t for t in living if t.role in (Role.ROMEO, Role.JULIET)]
            if len(living) == 2 and len(lovers) == 2:
                # Romeo and juliet stolen win
                winning_alignment = Alignment.NEUTRAL
                losers += winners
                winners = []
                for t in lovers:
                    t.person.win = True

        # Stalemate handling
        if self.stalemate_counter >= 3:
            winning_alignment = None
            losers += winners
            winners = []

        for t in self.town:
            if (t.person.win_alignment or t.person.alignment) == Alignment.NEUTRAL:
                if t.person.win:
                    winners.append(t)
                else:
                    losers.append(t)

        self.scene.set_actor_location([w.player.id for w in winners], AREA_WINNER)
        self.scene.set_actor_location([l.player.id for l in losers], AREA_LOSER)
        self.scene.filter_actors([t.player.id for t in self.town])

        results = [":video_game: The final results are:"]
        for n, t in enumerate(self.town, start=1):
            success = ":trophy:" if t in winners else ""
            results.append(f"{n}. {t.role.value[2]} **{t.name}** ({t.role.value[0]}) {success}")
        await self.discord.channel_town.send("\n".join(results))

        await asyncio.sleep(3)
        if winning_alignment == Alignment.VILLAGER:
            await self.discord.speak('town-win')
        elif winning_alignment == Alignment.WEREWOLF:
            await self.discord.speak('wolf-win')
        elif winning_alignment == Alignment.NEUTRAL:
            await self.discord.speak('lovers')
        else:
            await self.discord.speak('game-stalemate')
        await asyncio.sleep(5)
        
        await self.discord.reset()
 
        await asyncio.sleep(15)

        self.scene.set_scene(SceneType.FORK)
        self.scene.reset_actors()
        await self.discord.speak('end')

        self.state = State.LOBBY
        self.players = list()
        self.town = list()
        self.nomination_counter = 0
        self.stalemate_counter = 0
        
