import os
import random
import pygame
import pygame.freetype
import colorsys

def lerp(a, b, t):
    return type(a)((1.0 - t) * a + t * b)

def byte(a):
    return (int)(a * 255)

# Adapted from pygame wiki
def draw_wrapped_text(surface, text, color, rect, font):
    y = rect.top
    lineSpacing = -2

    # get the height of the font
    fontHeight = font.get_rect("Tg").h

    while text:
        i = 1

        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break

        # determine maximum width of line
        while font.get_rect(text[:i]).w < rect.width and i < len(text):
            i += 1

        # if we've wrapped the text, then adjust the wrap to the last word
        if i < len(text):
            b = text.rfind(" ", 0, i) + 1
            if b == 1:
                b = i - 1
            i = b

        # render the line and blit it to the surface
        image, area = font.render(text[:i], color)

        surface.blit(image, (rect.left, y))
        y += fontHeight + lineSpacing

        # remove the text we just blitted
        text = text[i:]

    return text

class GraphicFactory:
    def __init__(self):
        self.font = pygame.freetype.Font("GermaniaOne-Regular.ttf", 20)
        self.title = pygame.freetype.Font("GermaniaOne-Regular.ttf", 36)
        self.backgrounds = {}

    def load_background(self, filename, screen):
        path = os.path.join("backgrounds", f"{filename}.png")
        bg = pygame.image.load(path)
        bg.convert()
        bg = pygame.transform.scale(bg, (screen.get_width(), screen.get_height()))
        return bg

    def generate_name(self, name):
        outline = 2
        title, rect = self.font.render(name, (0x10, 0, 0))
        target = pygame.Surface((rect.w + 2*outline, rect.h + 2*outline), title.get_flags(), title)
        
        offsets = [(ox, oy) 
            for ox in range(0, 3*outline, outline)
            for oy in range(0, 3*outline, outline)
            if ox != outline or oy != outline]

        for ox, oy in offsets:
            target.blit(title, (ox, oy))

        title, rect = self.font.render(name, (0xFF, 0xFF, 0xFF))
        target.blit(title, (outline, outline))

        return target

    def generate_text_area(self, w, h, color, text):
        result = pygame.Surface((int(w), int(h)), pygame.HWSURFACE | pygame. SRCALPHA)
        draw_wrapped_text(result, text, color, pygame.Rect(0, 0, w, h), self.title)
        return result

    def draw_text(self, screen, x, y, text):
        self.title.render_to(screen, (x, y), text, (0xFF, 0xFF, 0xFF, 0xFF))

    def generate_skin_color(self):
        tone = random.random()
        return (lerp(0x90, 0xFF, tone), lerp(0x50, 0xE0, tone), lerp(0x20, 0xB0, tone), 0xFF)

    def generate_hair_color(self):
        red = random.randint(0, 255)
        green = random.randint(0, red)
        if green > red:
            red, green = green, red
        blue = 0
        age = random.random()
        if age > 0.6:
            age = min(1.0, (age - 0.6) * 5)
            red = lerp(red, 0xB0, age)
            green = lerp(green, 0xB0, age)
            blue = lerp(blue, 0xB0, age)
        return (red, green, blue, 0xFF)

    def generate_cloth_color(self):
        color = colorsys.hsv_to_rgb(random.random(), random.uniform(0.1, 0.4), random.uniform(0.3, 0.7))
        return (byte(color[0]), byte(color[1]), byte(color[2]), 0xFF)

    def generate_metal_color(self):
        color = colorsys.hsv_to_rgb(random.random(), random.uniform(0.15, 0.25), 0.75)
        return (byte(color[0]), byte(color[1]), byte(color[2]), 0xFF)

    def generate_layer_sequence(self, root):
        if root == "man":
            return [
                (self.generate_skin_color(), "body"),
                (self.generate_cloth_color(), random.choice(["pants-a", "pants-b"])),
                (self.generate_cloth_color(), random.choice(["shirt-a", "shirt-b", "shirt-c"])),
                (self.generate_hair_color(), random.choice(["hair-a", "hair-b", "hair-c"]))
            ]
        if root == "woman":
            return [
                (self.generate_skin_color(), "body"),
                (self.generate_cloth_color(), random.choice(["under-a", "under-b", "under-c"])),
                (self.generate_cloth_color(), random.choice(["over-a", "over-b"])),
                (self.generate_hair_color(), random.choice(["hair-a", "hair-b", "hair-c"]))
            ]
        if root == "robot":
            return [
                (self.generate_metal_color(), "body")
            ]
        
        return [((0xFF, 0xFF, 0xFF, 0xFF), "body")]

    def generate_person(self):
        # first, choose a random character

        person = random.choices(["man", "woman", "chicken", "robot"], [48, 48, 3, 1])[0]

        # build a sequence of layers / colors
        layers = self.generate_layer_sequence(person)

        result = list()
        for x in range(0, 7):
            image = None
            for layer in layers:
                path = os.path.join("sprites", person, f"{layer[1]}{x + 1}.png")
                ls = pygame.image.load(path)
                ls.convert_alpha()
                ls.fill(layer[0], special_flags=pygame.BLEND_MULT)
                if not image:
                    image = ls
                else:
                    image.blit(ls, (0, 0))
            image = pygame.transform.scale(image, (2 * image.get_width(), 2 * image.get_height()))
            result.append(image)

        for x in range(0, 7):
            result.append(pygame.transform.flip(result[x], True, False))

        return result

    def generate_ghost(self, color):
        result = list()
        for x in range(0, 7):
            path = os.path.join("sprites", f"ghost0{x + 1}.png")
            image = pygame.image.load(path)
            image.convert_alpha()
            image.fill(color, special_flags=pygame.BLEND_MULT)
            image = pygame.transform.scale(image, (2 * image.get_width(), 2 * image.get_height()))
            result.append(image)

        for x in range(0, 7):
            result.append(pygame.transform.flip(result[x], True, False))

        return result

