import random
from utils import get_number
from roles import Alignment, Role
from roleselection import possible_villager_roles

class Sequence(Enum):
    SCHEDULE = 0,
    INDEPENDENT = 1,
    POST_DAMAGE = 2

class Person:
    factory = {}
    def __init__(self):
        self.alignment = Alignment.VILLAGER
        self.win_alignment = None
        self.parity_alignment = None
        self.wolf_channel = False
        self.mason_channel = False
        self.dawn = False
        self.choice = -1
        self.proxy = None

    async def generic_begin(self, dm, town, preface, predicate):
        choices = [f'**{n}**: {t.name}' for n, t in enumerate(town, start=1) if predicate(t, self)]
        if choices:
            menu = '\n'.join(choices)
            await dm.send(f'{preface}\n{menu}')
        self.choice = -1
    
    async def generic_respond(self, dm, town, txt, message, predicate):
        i = get_number(txt) - 1
        if 0 <= i < len(town) and predicate(town[i], self):
            self.choice = i
            await dm.send(message.format(town[i].name))
        else:
            self.choice = -1
            await dm.send(f'You will abstain from acting tonight.')

    async def night_begin(self, dm, town):
        pass

    async def night_respond(self, dm, town, txt):
        if self.proxy:
            await self.proxy.proxy_night_respond(dm, town, txt)

    async def proxy_night_respond(self, dm, town, txt):
        pass

    async def night_end(self, seq, dm, town):
        pass

class Villager(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":person_shrugging: You are a **villager**. You have no special abilities. Good luck!"
Person.factory[Role.VILLAGER] = Villager

class Wolf(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.WEREWOLF
        self.wolf_channel = True
        self.intro = ":wolf: You are a **werewolf**. You have access to the werewolf private discussion. Each night, as a group, you will choose a player to kill."
Person.factory[Role.WOLF] = Wolf

class Seer(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":mage: You are a **seer**. You are on the villager team. Each night you may select one player, and will be told of their alignment (villager, werewolf, or neutral)."
        self.dawn = True

    targets = lambda a, me : a.person != me and a.alive

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, '**Please select a player to see...**', Seer.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You will see **{}**.", Seer.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Seer.targets(town[i], self):
            alignment = town[i].person.alignment
            if town[i].role == Role.LYCAN:
                alignment = Alignment.WEREWOLF
            await dm.send(f'You discover that {town[i].name} is {alignment.value[1]}.')
Person.factory[Role.SEER] = Seer

class Knight(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":shield: You are a **knight**. You are on the villager team. Each night you may select one player to protect. That player will be immune to a wolf attack at night. You can pick yourself. You cannot pick the same player two nights in a row."
        self.prior = None

    targets = lambda a, me : a.alive and a is not me.prior

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, '**Please select a player to guard:**', Knight.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You will protect **{}**.", Knight.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Knight.targets(town[i], self):
            self.prior = town[i]
            town[i].hp += 1
            if town[i] is self:
                await dm.send(f'You stand guard outside your own house.')
            else:
                await dm.send(f'You stand guard outside {town[i].name}\'s house.')
Person.factory[Role.KNIGHT] = Knight

class Gravedigger(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":headstone: You are a **gravedigger**. You are on the villager team. At night you may select one dead player to exhume. You will discover that player’s role."

    targets = lambda a, me : not a.alive

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, '**Please select a player to exhume...**', Gravedigger.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You will exhume **{}**.", Gravedigger.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Gravedigger.targets(town[i], self):
            await dm.send(f'You exhume {town[i].name}. In life, they were **{town[i].role.value[1]}**.')
Person.factory[Role.GRAVEDIGGER] = Gravedigger

class Mason(Person):
    def __init__(self):
        super().__init__()
        self.mason_channel = True
        self.intro = ":hammer_pick: You are a **mason**. You are on the villager team. You have no special abilities, at night you may access the masonic lodge channel with your masonic brothers. You can trust your fellow brethren."
Person.factory[Role.MASON] = Mason

class Tanner(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.NEUTRAL
        self.intro = ":poop: You are a **tanner**. As a neutral player, you are not on any team. Your objective is to be executed by popular vote during the day."
        self.win = False
Person.factory[Role.TANNER] = Tanner

class Minion(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.WEREWOLF
        self.wolf_channel = True
        self.intro = ":japanese_goblin: You are a **minion**. You are on the werewolf team. Each night you may select one player, and you will sense their role."
        self.dawn = True

    targets = lambda a, me : a.person != me and a.alive and not a.person.wolf_channel

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, '**Please select a player to sense...**', Minion.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You will sense **{}**.", Minion.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Minion.targets(town[i], self):
            await dm.send(f'You sense that {town[i].name} is {town[i].role.value[1]}.')
Person.factory[Role.MINION] = Minion

class Hunter(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":bow_and_arrow: You are a **hunter**. You are on the villager team. You have no special abilities, but you have a passive advantage: if executed during the day, the player who originally nominated you will be struck down by your bolt before you die."
Person.factory[Role.HUNTER] = Hunter

class Witch(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.NEUTRAL
        self.intro = ":woman_mage: You are a **witch**. As a neutral player, you are not on any team. Your objective is to survive until the end of the game. You may use your healing potions to protect yourself or others from death. Alternatively, you may use your poison vial to kill other players."
        self.heals = 2
        self.poisons = 2
        self.intent = 0
        self.win = True

    targets = lambda a, me : a.alive

    async def night_begin(self, dm, town):
        self.intent = 0
        self.choice = -1
        await self.generic_begin(dm, town, f'You have {self.heals} healing potions, and {self.poisons} poison vials. If you would like to use one, **please type h or p followed by a player to target...**', Witch.targets)

    async def night_respond(self, dm, town, txt):
        i = get_number(txt) - 1
        if txt.startswith('h') and 0 <= i < len(town) and Witch.targets(town[i], self):
            if self.heals == 0:
                await dm.send(f'You have run out of healing potions!')
            else:
                await dm.send(f'You will heal **{town[i].name}**.')
                self.intent = 1
                self.choice = i
        elif txt.startswith('p') and 0 <= i < len(town) and Witch.targets(town[i], self):
            if self.poisons == 0:
                await dm.send(f'You have run out of poison vials!')
            else:
                await dm.send(f'You will poison **{town[i].name}**.')
                self.intent = -1
                self.choice = i
        else:
            await dm.send(f'You will abstain from acting tonight.')
            self.choice = -1

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Witch.targets(town[i], self):
            town[i].hp += self.intent
            if self.intent == 1:
                self.heals -= 1
                if town[i].person is self:
                    await dm.send(f'You drink a healing potion.')
                else:
                    await dm.send(f'You slip a healing potion to {town[i].name}.')
            elif self.intent == -1:
                self.poisons -= 1
                if town[i].person is self:
                    await dm.send(f'You drink a poison vial... wait, what the hell? :thinking:')
                else:
                    await dm.send(f'You slip a poison vial to {town[i].name}.')
Person.factory[Role.WITCH] = Witch

class Bureaucrat(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":bar_chart: You are a **bureaucrat**. You are on the villager team. You get to know two roles that are in play."
        self.dawn = True
        self.complete = False

    async def night_begin(self, dm, town):
        if self.complete:
            return
        self.complete = True
        # assemble some roles that aren't trivial
        roles = [r.role for r in town if r.role not in (Role.VILLAGER, Role.WOLF, Role.CRIER, Role.BUREAUCRAT, Role.JUDGE)]
        roles = list(set(roles))
        random.shuffle(roles)
        if len(roles) >= 2:
            await dm.send(f"The following roles are in play: {roles[0].value[0]} and {roles[1].value[0]}.")
        elif len(roles) == 1:
            await dm.send(f"The following role is in play: {roles[0].value[0]}.")
        else:
            await dm.send(f"No interesting roles are in play.")
Person.factory[Role.BUREAUCRAT] = Bureaucrat

class Medium(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":crystal_ball: You are the **medium**. You are on the villager team. At night, you will be able to converse with the dead in the afterlife chat."
Person.factory[Role.MEDIUM] = Medium

class Crier(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.WEREWOLF
        self.wolf_channel = True
        self.intro = ":bell: You are the **town crier**. You are on the werewolf team. Each night you may give the bot a short message, and that will be relayed anonymously to all players first thing in the morning."
        self.dawn = True
        self.message = None

    async def night_begin(self, dm, town):
        self.message = None
        await dm.send("Please type the message you would like to give to the town. 140 characters maximum.")

    async def night_respond(self, dm, town, txt):
        if txt == "!clear":
            if self.message:
                self.message = None
                await dm.send("Message cleared")
        else:
            self.message = txt[:140]
            await dm.send("Message set. Type **!clear** to remove the message")
Person.factory[Role.CRIER] = Crier

class Fraud(Seer):
    def __init__(self):
        super().__init__()

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Seer.targets(town[i], self):
            # This is where it turns into nonsense
            alignment = random.choice([Alignment.VILLAGER, Alignment.VILLAGER, Alignment.VILLAGER, Alignment.WEREWOLF, Alignment.WEREWOLF, Alignment.NEUTRAL])
            await dm.send(f'You discover that {town[i].name} is {alignment.value[1]}.')
Person.factory[Role.FRAUD] = Fraud

class Judge(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":woman_judge: You are the **judge**. You are on the villager team. You have no special abilities, but you have a passive advantage: your vote counts as two normal votes. Your additional vote will be announced as coming from \'the judge\', so make sure you vote wisely."
Person.factory[Role.JUDGE] = Judge

class Priest(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":cross: You are the **priest**. You are on the villager team. Every night, another player who has no other night time activity will visit your confessional booth. You will be able to talk to them anonymously. They won't know who you are, and you won't know who they are."
        self.targets = []
        self.target = None

    async def night_begin(self, dm, town):
        if not self.targets:
            self.targets = [t for t in town if t.role in (Role.VILLAGER, Role.WOLF, Role.MASON, Role.HUNTER, Role.BUREAUCRAT, Role.MEDIUM, Role.JUDGE, Role.TRAITOR, Role.LYCAN, Role.HERO, Role.TANNER, Role.RAPSCALLION, Role.ROMEO, Role.JULIET)]
            random.shuffle(self.targets)
        if not self.targets:
            await dm.send("Nobody visits your booth...")
        while self.targets and not self.target:
            t = self.targets[0]
            if t.alive and not t.person.proxy:
                self.dm = dm
                self.target = t
                t.person.proxy = self
                await dm.send(":pray: Someone steps into your booth.")
                await t.player.dm_channel.send(":pray: You step into a confessional booth.")
            self.targets = self.targets[1:]

    async def night_respond(self, dm, town, txt):
        if not self.target:
            return
        await self.target.player.dm_channel.send(f":lips: {txt}")

    async def proxy_night_respond(self, dm, town, txt):
        await self.dm.send(f":lips: {txt}")

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        if self.target:
            self.target.proxy = None
            self.target = None
Person.factory[Role.PRIEST] = Priest

class Detective(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":detective: You are a **detective**. You are on the villager team. Each night you may select one player, and will be told if that player performs any night activity (via wolftown-helper) as part of their role."
        self.dawn = True

    targets = lambda a, me : a.person != me and a.alive

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, '**Please select a player to investigate...**', Detective.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You will investigate **{}**.", Detective.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Detective.targets(town[i], self):
            if town[i].role in (Role.SEER, Role.KNIGHT, Role.GRAVEDIGGER, Role.FRAUD, Role.PRIEST, Role.DETECTIVE, Role.VIGILANTE, Role.WITCH, Role.KING, Role.GAMBLER, Role.MINION, Role.CRIER, Role.ALPHA):
                await dm.send(f'You discover that {town[i].name} is especially active at night.')
            else:
                await dm.send(f'You discover that {town[i].name} rests at night without incident.')
Person.factory[Role.DETECTIVE] = Detective

class Traitor(Person):
    def __init__(self):
        super().__init__()
        self.win_alignment = Alignment.WEREWOLF
        self.intro = ":supervillain: You are a **traitor**. You are on the villager team, but you win with the werewolf team. You know who the wolves are, but you do not have access to the werewolf private discussion."
        self.dawn = True
        self.complete = False

    async def night_begin(self, dm, town):
        if self.complete:
            return
        self.complete = True
        roles = [f"**{r.name}**" for r in town if r.person.alignment == Alignment.WEREWOLF]
        roles.sort()
        roles_txt = ", ".join(roles)
        await dm.send(f"The following players are werewolves: {roles_txt}.")
Person.factory[Role.TRAITOR] = Traitor

class King(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.NEUTRAL
        self.intro = ":crown: You are the **king**. As a neutral player, you are not on any team. Your objective is to survive until the end of the game. At night, you may investigate specific villager roles to find the name of a player with that role (if applicable). You have a passive ability to pardon yourself from execution, but only once."
        self.pardon = True
        self.win = True

    options = [Role.SEER, Role.KNIGHT, Role.HUNTER, Role.MASON, Role.GRAVEDIGGER, Role.DETECTIVE]

    async def night_begin(self, dm, town):
        choices = [f'**{n}**: {t}' for n, t in enumerate(King.options, start=1)]
        choices_txt = '\n'.join(choices)
        await dm.send(f'Select a role to investigate...\n{choices_txt}')
        self.choice = -1

    async def night_respond(self, dm, town, txt):
        i = get_number(txt) - 1
        if 0 <= i < len(King.options):
            self.choice = i
            await dm.send(f"You will investigate **{King.options[i].value[1]}**.")
        else:
            self.choice = -1
            await dm.send(f'You will abstain from acting tonight.')

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(King.options):
            role = King.options[i]
            targets = [t for t in town if t.alive and t.role == role]
            if not targets:
                await dm.send(f'You do not discover {role.value[1]}.')
                return
            target = random.choice(targets)
            await dm.send(f'You discover that {target.name} is {role.value[1]}.')
Person.factory[Role.KING] = King

class Lycan(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":bearded_person: You are a **lycan**. You are on the villager team. Due to your overwhelming body hair, the seer will perceive you as a werewolf."
Person.factory[Role.LYCAN] = Lycan

class Hero(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":superhero: You are a **hero**. You are on the villager team. Due to your extremely heroic attitude, you will survive an attack at night, only to die from your injuries the following night."
        self.dying = False

    async def night_begin(self, dm, town):
        if self.dying:
            me = [t for t in town if t.person is self]
            if me:
                me[0].hp -= 100
                await dm.send(f'You will die from your wounds tonight.')
Person.factory[Role.HERO] = Hero

class Vigilante(Person):
    def __init__(self):
        super().__init__()
        self.intro = ":person_fencing: You are a **vigilante**. You are on the villager team. At night, you may attack another player. If you attack a player who is on the villager team, you will die of shame."

    targets = lambda a, me : a.person != me and a.alive

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, '**Please select a player to attack...**', Vigilante.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You will attack **{}**.", Vigilante.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        i = self.choice
        if 0 <= i < len(town) and Vigilante.targets(town[i], self):
            await dm.send(f'You attack {town[i].name}...')
            town[i].hp -= 1
            if town[i].person.alignment == Alignment.VILLAGER:
                me = [t for t in town if t.person is self]
                if me:
                    me[0].hp -= 100
Person.factory[Role.VIGILANTE] = Vigilante

class Rapscallion(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.WEREWOLF
        self.wolf_channel = True
        self.intro = ":chart_with_downwards_trend: You are a **rapscallion**. You are on the werewolf team. You get to know a villager role that is not in play, but is a valid role for the current player count."
        self.dawn = True
        self.complete = False

    async def night_begin(self, dm, town):
        if self.complete:
            return
        self.complete = True
        possible = possible_villager_roles(len(town))
        current = set([t.role for t in town])
        roles = [p for p in possible if p not in current and p not in (Role.TRAITOR, Role.JUDGE)]
        roles = list(set(roles))
        random.shuffle(roles)
        if len(roles) > 0:
            await dm.send(f"The following role is not in play: {roles[0].value[0]}.")
        else:
            await dm.send(f"There are no valid villager roles which are not in play.")
Person.factory[Role.RAPSCALLION] = Rapscallion

class Alpha(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.WEREWOLF
        self.wolf_channel = True
        self.intro = ":shark: You are the **alpha**. You are on the werewolf team. At night, only once, you may select someone to personally kill."
        self.attacks = 1

    targets = lambda a, me : a.person != me and a.alive

    async def night_begin(self, dm, town):
        if self.attacks > 0:
            await self.generic_begin(dm, town, '**If you wish, please select a player to attack...**', Alpha.targets)

    async def night_respond(self, dm, town, txt):
        if self.attacks > 0:
            await self.generic_respond(dm, town, txt, "You will attack **{}**.", Alpha.targets)

    async def night_end(self, seq, dm, town):
        if seq != Sequence.INDEPENDENT:
            return
        if self.attacks == 0:
            return
        i = self.choice
        if 0 <= i < len(town) and Alpha.targets(town[i], self):
            await dm.send(f'You attack {town[i].name}...')
            town[i].hp -= 1
            self.attacks -= 1
Person.factory[Role.ALPHA] = Alpha

class Lover(Person):
    def __init__(self):
        super().__init__()
        self.lover = None
        self.dawn = True
        self.alignment = Alignment.NEUTRAL
        self.win = False

    async def night_begin(self, dm, town):
        if not self.lover:
            lovers = [t for t in town if t.person is not self and isinstance(t.person, Lover)]
            if len(lovers) == 1:
                self.lover = lovers[0]
                await dm.send(f"Your secret lover is **{self.lover.name}**.")
        else:
            if not self.lover.alive:
                me = [t for t in town if t.person is self]
                if me:
                    me[0].hp -= 100
                    await dm.send(f'You will die tonight due to a broken heart.')

class Romeo(Lover):
    def __init__(self):
        super().__init__()
        self.parity_alignment = Alignment.VILLAGER
        self.intro = ":heart: You are **romeo**. As a neutral player, you are not on any team, but you appear to be on the villager team. Your objective is for you and your lover Juliet (a werewolf) to be the last two players alive. If one of you dies, the other will die the next night."
Person.factory[Role.ROMEO] = Romeo

class Juliet(Lover):
    def __init__(self):
        super().__init__()
        self.parity_alignment = Alignment.WEREWOLF
        self.wolf_channel = True
        self.intro = ":anatomical_heart: You are **juliet**. As a neutral player, you are not on any team, but you appear to be on the werewolf team. Your objective is for you and your lover Romeo (a villager) to be the last two players alive. If one of you dies, the other will die the next night."
Person.factory[Role.JULIET] = Juliet

class Gambler(Person):
    def __init__(self):
        super().__init__()
        self.alignment = Alignment.NEUTRAL
        self.dawn = True
        self.intro = ":game_die: You are a **gambler**. As a neutral player, you are not on any team. Your objective is to correctly predict two times who will be lynched during the day. You are tough enough to survive one attack at night."
        self.score = 0
        self.mark = None
        self.protection = 1
        self.win = False

    targets = lambda a, me : a.alive

    async def night_begin(self, dm, town):
        await self.generic_begin(dm, town, f'Your current score is {self.score}. **Please bet on who will be lynched during the day...**', Gambler.targets)

    async def night_respond(self, dm, town, txt):
        await self.generic_respond(dm, town, txt, "You bet that **{}** will be lynched.", Gambler.targets)

    async def night_end(self, seq, dm, town):
        if seq == Sequence.INDEPENDENT:
            i = self.choice
            self.mark = None
            if 0 <= i < len(town) and Gambler.targets(town[i], self):
                self.mark = town[i]
        elif seq == Sequence.POST_DAMAGE:
            me = [t for t in town if t.person is self]
            if me and me[0].hp < 1 and self.protection > 0:
                self.protection -= 1
                me[0].hp = 1
                await dm.send(f'You were attacked at night, but managed to fight off the attacker.')
Person.factory[Role.GAMBLER] = Gambler
