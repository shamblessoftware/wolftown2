from enum import Enum

# Alignments, name, name with determiner, color
class Alignment(Enum):
    VILLAGER = ("villager", "a villager", (0xC0, 0xFF, 0xF0, 0xFF))
    WEREWOLF = ("werewolf", "a werewolf", (0xD0, 0x48, 0x38, 0xFF))
    NEUTRAL = ("neutral", "neutral", (0x68, 0x98, 0x68, 0xFF))

# Enum of role, role with determiner, emoji and team affiliation (just general categorization)
class Role(Enum):
    VILLAGER = ("villager", "a villager", ":person_shrugging:", Alignment.VILLAGER)
    SEER = ("seer", "a seer", ":mage:", Alignment.VILLAGER)
    KNIGHT = ("knight", "a knight", ":shield:", Alignment.VILLAGER)
    GRAVEDIGGER = ("gravedigger", "a gravedigger", ":headstone:", Alignment.VILLAGER)
    MASON = ("mason", "a mason", ":hammer_pick:", Alignment.VILLAGER)
    HUNTER = ("hunter", "a hunter", ":bow_and_arrow:", Alignment.VILLAGER)
    BUREAUCRAT = ("bureaucrat", "a bureaucrat", ":bar_chart:", Alignment.VILLAGER)
    MEDIUM = ("medium", "the medium", ":crystal_ball:", Alignment.VILLAGER)
    FRAUD = ("fraud", "a fraud", ":briefs:", Alignment.VILLAGER)
    JUDGE = ("judge", "the judge", ":woman_judge:", Alignment.VILLAGER)
    PRIEST = ("priest", "the priest", ":cross:", Alignment.VILLAGER)
    DETECTIVE = ("detective", "a detective", ":detective:", Alignment.VILLAGER)
    LYCAN = ("lycan", "a lycan", ":bearded_person:", Alignment.VILLAGER)
    HERO = ("hero", "a hero", ":superhero:", Alignment.VILLAGER)
    VIGILANTE = ("vigilante", "a vigilante", ":person_fencing:", Alignment.VILLAGER)

    TANNER = ("tanner", "a tanner", ":poop:", Alignment.NEUTRAL)
    WITCH = ("witch", "a witch", ":woman_mage:", Alignment.NEUTRAL)
    KING = ("king", "the king", ":crown:", Alignment.NEUTRAL)
    ROMEO = ("romeo", "romeo", ":heart:", Alignment.NEUTRAL)
    JULIET = ("juliet", "juliet", ":anatomical_heart:", Alignment.NEUTRAL)
    GAMBLER = ("gambler", "a gambler", ":game_die:", Alignment.NEUTRAL)

    WOLF = ("werewolf", "a werewolf", ":wolf:", Alignment.WEREWOLF)
    MINION = ("minion", "a minion", ":japanese_goblin:", Alignment.WEREWOLF)
    CRIER = ("town crier", "the town crier", ":bell:", Alignment.WEREWOLF)
    RAPSCALLION = ("rapscallion", "a rapscallion", ":chart_with_downwards_trend:", Alignment.WEREWOLF)
    TRAITOR = ("traitor", "a traitor", ":supervillain:", Alignment.WEREWOLF)
    ALPHA = ("alpha", "the alpha", ":shark:", Alignment.WEREWOLF)

