import random
import math
import numpy
from roles import Role

# Strength scaling
powers = {
    #town powers
    Role.VILLAGER: 1.0,
    Role.SEER: 3.5,
    Role.KNIGHT: 1.3,
    Role.GRAVEDIGGER: 1.1,
    Role.MASON: 1.6,
    Role.HUNTER: 1.2,
    Role.BUREAUCRAT: 1.1,
    Role.MEDIUM: 1.2,
    Role.FRAUD: 0.6,
    Role.JUDGE: 1.1,
    Role.PRIEST: 1.8,
    Role.DETECTIVE: 1.6,
    Role.TRAITOR: -3.5,
    Role.LYCAN: 0.4,
    Role.HERO: 1.2,
    Role.VIGILANTE: 1.4,

    #neuts?
    Role.TANNER: -0.8,
    Role.WITCH: 0.0,
    Role.KING: -0.2,
    Role.ROMEO: -0.6,
    Role.JULIET: -0.6,
    Role.GAMBLER: -0.2,

    #wolf powers
    Role.WOLF: 3,
    Role.MINION: 7,
    Role.CRIER: 5,
    Role.RAPSCALLION: 4,
    Role.ALPHA: 8
}

max_neutrals = {
    4: 0,
    5: 0,
    6: 0,
    7: 1,
    8: 1,
    9: 1,
    10: 2,
    11: 2,
    12: 2,
    13: 3,
    14: 3,
    15: 3,
    16: 4
}

neutral_probabilities = {
    4: [],
    5: [],
    6: [],
    7: [
        (Role.TANNER, 0.1)
    ],
    8: [
        (Role.TANNER, 0.2),
        (Role.WITCH, 0.2)
    ],
    9: [
        (Role.TANNER, 0.3),
        (Role.WITCH, 0.3),
        (Role.GAMBLER, 0.08)
    ],
    10: [
        (Role.TANNER, 0.3, 0.1),
        (Role.WITCH, 0.3),
        (Role.GAMBLER, 0.08),
        (Role.KING, 0.08)
    ],
    11: [
        (Role.TANNER, 0.3, 0.2),
        (Role.WITCH, 0.3, 0.1),
        (Role.GAMBLER, 0.08),
        (Role.KING, 0.12)
    ],
    12: [
        (Role.TANNER, 0.3, 0.3),
        (Role.WITCH, 0.3, 0.2),
        (Role.GAMBLER, 0.1),
        (Role.KING, 0.13),
        (Role.ROMEO, 0.05)
    ],
    13: [
        (Role.TANNER, 0.3, 0.3),
        (Role.WITCH, 0.3, 0.25),
        (Role.GAMBLER, 0.1),
        (Role.KING, 0.14),
        (Role.ROMEO, 0.06)
    ],
    14: [
        (Role.TANNER, 0.3, 0.3),
        (Role.WITCH, 0.3, 0.3),
        (Role.GAMBLER, 0.1),
        (Role.KING, 0.15),
        (Role.ROMEO, 0.07)
    ],
    15: [
        (Role.TANNER, 0.3, 0.3),
        (Role.WITCH, 0.3, 0.3),
        (Role.GAMBLER, 0.11),
        (Role.KING, 0.16),
        (Role.ROMEO, 0.08)
    ],
    16: [
        (Role.TANNER, 0.3, 0.3),
        (Role.WITCH, 0.3, 0.3),
        (Role.GAMBLER, 0.12),
        (Role.KING, 0.16),
        (Role.ROMEO, 0.08)
    ]
}

role_probabilities = {
    3: [
        (Role.SEER, 0.95),
        (Role.KNIGHT, 0.5),
    ],
    4: [
        (Role.SEER, 0.95),
        (Role.KNIGHT, 0.5),
        (Role.DETECTIVE, 0.08),
    ],
    5: [
        (Role.SEER, 0.95),
        (Role.FRAUD, 0.1),
        (Role.KNIGHT, 0.75),
        (Role.DETECTIVE, 0.1),
        (Role.HUNTER, 0.4),
        (Role.BUREAUCRAT, 0.3)
    ],
    6: [
        (Role.SEER, 0.95, 0.04),
        (Role.FRAUD, 0.12),
        (Role.KNIGHT, 0.8),
        (Role.DETECTIVE, 0.12),
        (Role.HUNTER, 0.5),
        (Role.BUREAUCRAT, 0.3),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.1),
        (Role.MASON, 0.2, 1.0)
    ],
    7: [
        (Role.SEER, 0.95, 0.05),
        (Role.FRAUD, 0.15),
        (Role.KNIGHT, 0.8, 0.05),
        (Role.DETECTIVE, 0.15),
        (Role.HUNTER, 0.6, 0.05),
        (Role.BUREAUCRAT, 0.3),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.2),
        (Role.MASON, 0.22, 1.0),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.05),
        (Role.HERO, 0.1),
        (Role.VIGILANTE, 0.1),
        (Role.LYCAN, 0.02)
    ],
    8: [
        (Role.SEER, 0.95, 0.06),
        (Role.FRAUD, 0.16),
        (Role.KNIGHT, 0.8, 0.07),
        (Role.DETECTIVE, 0.2),
        (Role.HUNTER, 0.6, 0.1),
        (Role.BUREAUCRAT, 0.4, 0.02),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.25),
        (Role.MASON, 0.25, 1.0),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.05),
        (Role.HERO, 0.11),
        (Role.VIGILANTE, 0.15),
        (Role.LYCAN, 0.02)
    ],
    9: [
        (Role.SEER, 0.95, 0.07),
        (Role.FRAUD, 0.17),
        (Role.KNIGHT, 0.8, 0.1),
        (Role.DETECTIVE, 0.25, 0.01),
        (Role.HUNTER, 0.6, 0.15),
        (Role.BUREAUCRAT, 0.45, 0.05),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.25),
        (Role.MASON, 0.25, 1.0),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.05),
        (Role.HERO, 0.12),
        (Role.VIGILANTE, 0.2),
        (Role.LYCAN, 0.03)
    ],
    10: [
        (Role.SEER, 0.95, 0.08),
        (Role.FRAUD, 0.18),
        (Role.KNIGHT, 0.8, 0.1),
        (Role.DETECTIVE, 0.3, 0.02),
        (Role.HUNTER, 0.6, 0.2),
        (Role.BUREAUCRAT, 0.5, 0.07),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.25),
        (Role.MASON, 0.25, 1.0, 0.05),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.07),
        (Role.HERO, 0.15),
        (Role.VIGILANTE, 0.3),
        (Role.LYCAN, 0.04)
    ],
    11: [
        (Role.SEER, 0.95, 0.1),
        (Role.FRAUD, 0.2),
        (Role.KNIGHT, 0.8, 0.2),
        (Role.DETECTIVE, 0.35, 0.05),
        (Role.HUNTER, 0.6, 0.3),
        (Role.BUREAUCRAT, 0.55, 0.1),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.25),
        (Role.MASON, 0.25, 1.0, 0.07),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.08),
        (Role.HERO, 0.15),
        (Role.VIGILANTE, 0.4, 0.05),
        (Role.LYCAN, 0.05),
        (Role.TRAITOR, 0.02)
    ],
    12: [
        (Role.SEER, 0.95, 0.1),
        (Role.FRAUD, 0.2),
        (Role.KNIGHT, 0.8, 0.25),
        (Role.DETECTIVE, 0.4, 0.07),
        (Role.HUNTER, 0.6, 0.4),
        (Role.BUREAUCRAT, 0.6, 0.2),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.25),
        (Role.MASON, 0.25, 1.0, 0.1),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.1),
        (Role.HERO, 0.15),
        (Role.VIGILANTE, 0.5, 0.07),
        (Role.LYCAN, 0.07),
        (Role.TRAITOR, 0.03)
    ],
    13: [
        (Role.SEER, 0.95, 0.12),
        (Role.FRAUD, 0.2),
        (Role.KNIGHT, 0.8, 0.25),
        (Role.DETECTIVE, 0.4, 0.1),
        (Role.HUNTER, 0.6, 0.5),
        (Role.BUREAUCRAT, 0.6, 0.3),
        (Role.JUDGE, 0.15),
        (Role.GRAVEDIGGER, 0.25),
        (Role.MASON, 0.25, 1.0, 0.1),
        (Role.MEDIUM, 0.1),
        (Role.PRIEST, 0.1),
        (Role.HERO, 0.15),
        (Role.VIGILANTE, 0.5, 0.1),
        (Role.LYCAN, 0.1),
        (Role.TRAITOR, 0.05)
    ]
}

wolf_options = [Role.MINION, Role.CRIER, Role.RAPSCALLION, Role.ALPHA]


def select_from_probability_list(prob):
    result = []
    for t in prob:
        for i in range(1, len(t)):
            if numpy.random.rand() > t[i]:
                break
            result.append(t[0])
    return result


def reduce_list(roles, n):
    while len(roles) > n:
        numpy.random.shuffle(roles)
        roles.pop(0)


def pad_list(roles, role, n):
    roles += [role] * max(0, n - len(roles))


def select_roles(n):
    neutrals = select_from_probability_list(neutral_probabilities[n])
    reduce_list(neutrals, max_neutrals[n])
    neutral_count = len(neutrals)

    wolf_count = math.floor(math.sqrt(n - neutral_count - 3))
    if Role.ROMEO in neutrals:
        neutrals.append(Role.JULIET)
        wolf_count -= 1

    villager_count = n - wolf_count - neutral_count

    town = []
    while True:
        town = select_from_probability_list(role_probabilities[villager_count])
        reduce_list(town, villager_count)
        pad_list(town, Role.VILLAGER, villager_count)

        if town.count(Role.MASON) == 1:
            continue
        if town.count(Role.VILLAGER) > 3:
            continue
        break

    town += neutrals

    power = sum([powers[t] for t in town])

    wolf_source = wolf_options + [Role.WOLF] * wolf_count
    wolves = []
    best = None
    for i in range(12):
        guess = numpy.random.choice(wolf_source, size=wolf_count, replace=False)
        diff = abs(sum([powers[t] for t in guess]) - power)
        if not best or diff < best:
            wolves = guess
            best = diff

    return town + list(wolves)


def possible_villager_roles(n):
    wolves = math.floor(math.sqrt(n - 3))
    villagers = n - wolves
    village_roles = [x[0] for x in role_probabilities[villagers]]
    return village_roles


def possible_roles(n):
    neutral_roles = [x[0] for x in neutral_probabilities[n]]
    if Role.ROMEO in neutral_roles:
        neutral_roles.append(Role.JULIET)

    wolf_roles = [Role.WOLF] + wolf_options

    return neutral_roles + wolf_roles + possible_villager_roles(n)
