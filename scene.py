import math
import random
import pygame
import itertools
from enum import Enum

from actor import Actor
from graphicfactory import GraphicFactory

FPS = 30.0
SCREENSIZE = [640, 480]

AREA_OFF_LEFT = (-0.25, 0.29, 0, 0.42)
AREA_OFF_RIGHT = (1.25, 0.29, 0, 0.42)
AREA_OFF_TOP = (0.34, -0.07, 0.32, 0)
AREA_OFF_BOTTOM = (0.34, 1.5, 0.32, 0)

AREA_FORK = (0.31, 0.28, 0.38, 0.49)

AREA_TOWN = (0.16, 0.25, 0.68, 0.58)
AREA_NOMINATED = (0.5, 0.25, 0, 0)
AREA_LYNCH = (0.48, 0.22, 0.04, 0.06)
AREA_CROWD = (0.4, 0.5, 0.2, 0.28)
AREA_VOTE_YES = (0.05, 0.3, 0.2, 0.42)
AREA_VOTE_NO = (0.75, 0.3, 0.2, 0.42)

AREA_WINNER = (0.1, 0.25, 0.8, 0.2)
AREA_LOSER = (0.1, 0.65, 0.8, 0.2)

def screenspace_area(a):
    return pygame.Rect(a[0] * SCREENSIZE[0], a[1] * SCREENSIZE[1], a[2] * SCREENSIZE[0], a[3] * SCREENSIZE[1])

class SceneType(Enum):
    FORK = ("fork", 0)
    DAWN = ("dawn", 0)
    TOWN = ("town", 0)
    MOON = ("moon", 0)
    GRAVEYARD = ("graveyard", 0)
    BLACK = ("black", 0)
    WINNERS = ("win", 0)
    SCROLL = ("scroll", 0)

class Renderable:
    def __init__(self, surface, x, y):
        self.surface = surface
        self.x = x
        self.y = y

class Scene:
    def __init__(self):
        self.running = True
        self.g_factory = None
        self.actors = {}
        self.visible_actors = []
        self.background = None
        self.text = []
        self.timer = -1

    def graphic_factory(self):
        if not self.g_factory:
            self.g_factory = GraphicFactory()
        return self.g_factory

    def loop(self):
        pygame.init()
        icon = pygame.image.load("icon-wolftown.png")
        pygame.display.set_icon(icon)
        pygame.display.set_caption("Wolftown")
        self.screen = pygame.display.set_mode(SCREENSIZE)
        
        clock = pygame.time.Clock()
        while self.running:
            self.update()
            self.render()
            pygame.display.flip()
            clock.tick(FPS)

        pygame.quit()

    def update(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
        for actor in self.actors.values():
            actor.update(1.0/FPS)
        for one, two in itertools.combinations(self.actors.values(), 2):
            dx = two.x - one.x
            dy = two.y - one.y
            sqr = dx*dx + dy*dy
            exclusion = 20
            if sqr < exclusion*exclusion:
                dist = math.sqrt(sqr)
                pushout = exclusion - dist
                scale = pushout / (2 * dist)
                one.x -= dx * scale
                one.y -= dy * scale
                two.x += dx * scale
                two.y += dy * scale

        self.timer -= 1.0/FPS

    def render(self):
        self.screen.fill((0, 0, 0))
        if self.background:
            self.screen.blit(self.background, (0, 0))
        self.visible_actors.sort(key=lambda x: x.y)
        for r in self.visible_actors:
            r.render(self.screen)
        for t in self.text:
            self.screen.blit(t.surface, (t.x, t.y))
        if self.timer > 0:
            self.graphic_factory().draw_text(self.screen, 0.9 * SCREENSIZE[0], 0.9 * SCREENSIZE[1], f"{int(self.timer)}")


    def add_actor(self, uid, name, area):
        if not uid in self.actors:
            self.actors.update({uid: Actor(self.graphic_factory(), name)})
            self.visible_actors.append(self.actors[uid])
            self.actors[uid].set_area(screenspace_area(area), False)

    def filter_actors(self, valid):
        self.visible_actors = [self.actors[key] for key in valid]

    def set_actor_action(self, uids, action):
        for uid in uids:
            if uid in self.actors:
                self.actors[uid].start_action(action)

    def kill_actor(self, uid, color):
        if uid in self.actors:
            self.actors[uid].become_ghost(self.graphic_factory(), color)

    def reset_actors(self):
        self.actors = {}
        self.visible_actors = list()

    def set_actor_location(self, uids, area):
        for uid in uids:
            if uid in self.actors:
                self.actors[uid].set_area(screenspace_area(area), False)

    def set_actor_walk_area(self, uids, area):
        for uid in uids:
            if uid in self.actors:
                self.actors[uid].set_area(screenspace_area(area), True)

    def add_text(self, x, y, w, h, color, text):
        surface = self.graphic_factory().generate_text_area(w * SCREENSIZE[0], h * SCREENSIZE[1], color, text)
        self.text.append(Renderable(surface, x * SCREENSIZE[0], y * SCREENSIZE[1]))

    def clear_text(self):
        self.text = []

    def set_timer(self, time):
        self.timer = time

    def get_timer(self):
        return self.timer

    def clear_timer(self):
        self.timer = -1

    def set_scene(self, bg):
        if isinstance(bg, SceneType):
            self.background = self.graphic_factory().load_background(bg.value[0], self.screen)

