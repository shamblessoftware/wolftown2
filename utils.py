import re

def get_number(s):
    return int(re.search(r'\d+', f'{s} 0').group())

