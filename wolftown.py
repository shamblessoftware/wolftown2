import os
import asyncio
from dotenv import load_dotenv
from threading import Thread
from dataclasses import dataclass

from bot import Bot
from scene import Scene

load_dotenv()

# Read environment file

@dataclass
class Env:
    DISCORD_TOKEN: str = os.getenv('DISCORD_TOKEN')
    DISCORD_GUILD: str = os.getenv('DISCORD_GUILD')
    CHANNEL_VOICE: str = os.getenv('CHANNEL_VOICE')
    CHANNEL_TOWN: str = os.getenv('CHANNEL_TOWN')
    CHANNEL_MASON: str = os.getenv('CHANNEL_MASON')
    CHANNEL_WOLF: str = os.getenv('CHANNEL_WOLF')
    CHANNEL_DEAD: str = os.getenv('CHANNEL_DEAD')
    GM_ROLE: str = os.getenv('GM_ROLE')

env = Env()

scene = Scene()
client = Bot(scene, env)

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)

loop.create_task(client.start(env.DISCORD_TOKEN))

thread = Thread(target=loop.run_forever)
thread.start()

scene.loop()

loop.call_soon_threadsafe(loop.stop)
thread.join()

